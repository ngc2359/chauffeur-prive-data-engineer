"""
test_transform
"""
# pylint: disable=too-few-public-methods,line-too-long,invalid-name

import io

from pandas import DataFrame
from pandas.testing import assert_frame_equal

from cp_datawarehouse.etl.transform import (
    clean_users_csv,
    filter_rides_csv
)


def test_clean_users_csv():
    """
    Test the cleaning of users CSV data:
    * should replace misspelled "platinium" values with "platinum"
    """

    users_csv = io.StringIO(
        'user_id,loyalty_status,loyalty_status_txt\n1,0,red\n2,1,silver\n3,2,gold\n4,3,platinium\n5,3,platinum')

    clean_users_df = clean_users_csv(users_csv)

    assert_frame_equal(
        clean_users_df.sort_index(axis=1),
        DataFrame({
            'user_id': [1, 2, 3, 4, 5],
            'loyalty_status': [0, 1, 2, 3, 3],
            'loyalty_status_txt': ['red', 'silver', 'gold', 'platinum', 'platinum'],
        }).sort_index(axis=1),
        check_names=True
    )


def test_filter_rides_csv():
    """
    Test the filtering of rides CSV data:
    * should filter zipcodes to only keep Ile-de-France data
    """

    rides_csv = io.StringIO(
        'ride_id,user_id,from_zipcode,to_zipcode,state,quote_date,completed_date,price_nominal,loyalty_points_earned\n'
        '3d08e7b630d78e2272f36e5c6624a108,7fb2935d9b84e0ddb5b95b05a04d224e, 75001,75017,not_completed,2018-04-11 00:15:55.404,,4.00,0\n'
        '01b07272321ef0c57d6ba08518008113,4acfbfe47295ffa624184ae7d0601246,92200,mad,completed,2018-05-03 10:35:50.551,2018-05-03 11:15:10.232,5.74,13\n'
        'c8dedee7af6cf87dad2ed65df81dc2e1,8af8011d328c7e230dad6dc6023c653f,69125,69600,completed,2018-04-28 11:01:32.45,2018-04-28 11:42:15.375,10.78,11\n'
        '51b2610e42c21990c3731335fe961108,3f6286c2b7bcc79831f8ab2cf700c411,69125,75010,not_completed,2018-03-17 21:23:44.798,,4.00,0')

    filtered_rides_df = filter_rides_csv(rides_csv).fillna('')

    assert_frame_equal(
        filtered_rides_df.sort_index(axis=1),
        DataFrame({
            'ride_id': ['3d08e7b630d78e2272f36e5c6624a108'],
            'user_id': ['7fb2935d9b84e0ddb5b95b05a04d224e'],
            'from_zipcode': [' 75001'],
            'to_zipcode': ['75017'],
            'state': ['not_completed'],
            'quote_date': ['2018-04-11 00:15:55.404'],
            'completed_date': [''],
            'price_nominal': ['4.00'],
            'loyalty_points_earned': ['0']
        }).sort_index(axis=1),
        check_names=True
    )
