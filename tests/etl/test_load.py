"""
test_load
"""
# pylint: disable=too-few-public-methods,line-too-long,invalid-name

import io

import pandas as pd
import psycopg2
from pandas.testing import assert_frame_equal

from cp_datawarehouse.config.base import config
from cp_datawarehouse.etl.load import (
    insert_users_list,
    insert_rides_df
)
from tests.tools.db_init import initialize_database

CONFIG = config()


def test_insert_users_list():
    """
    Test INSERT of a dummy CSV into users database table
    """

    initialize_database(drop=True)

    users_list = [
        ('user_id', 'loyalty_status', 'loyalty_status_txt'),
        (1, 0, 'red'),
        (3, 3, 'platinum'),
        (4, 1, 'silver'),
        (2, 2, 'gold'),
    ]

    insert_users_list(users_list)

    conn = psycopg2.connect(CONFIG["postgres_url"])

    with conn:
        cur = conn.cursor()
        cur.execute("SELECT * FROM cp_datawarehouse.users")
        result = cur.fetchall()

    # Skip CSV header first line
    assert result.sort() == users_list[1:].sort()


def test_insert_rides_df():
    """
    Test INSERT of a pandas DataFrame into rides database table
    """

    initialize_database(drop=True)

    rides_dtype = {
        'ride_id': str,
        'user_id': str,
        'from_zipcode': str,
        'to_zipcode': str,
        'state': str,
        'quote_date': str,
        'completed_date': str,
        'price_nominal': float,
        'loyalty_points_earned': int
    }

    rides_csv = io.StringIO(
        'ride_id,user_id,from_zipcode,to_zipcode,state,quote_date,completed_date,price_nominal,loyalty_points_earned\n'
        '3d08e7b630d78e2272f36e5c6624a108,7fb2935d9b84e0ddb5b95b05a04d224e, 75001,75017,not_completed,2018-04-11 00:15:55.404,,4.00,0\n'
        '01b07272321ef0c57d6ba08518008113,4acfbfe47295ffa624184ae7d0601246,92200,mad,completed,2018-05-03 10:35:50.551,2018-05-03 11:15:10.232,5.74,13\n'
        'c8dedee7af6cf87dad2ed65df81dc2e1,8af8011d328c7e230dad6dc6023c653f,69125,69600,completed,2018-04-28 11:01:32.45,2018-04-28 11:42:15.375,10.78,11\n'
        '51b2610e42c21990c3731335fe961108,3f6286c2b7bcc79831f8ab2cf700c411,69125,75010,not_completed,2018-03-17 21:23:44.798,,4.00,0')

    rides_df = pd.read_csv(
        rides_csv,
        delimiter=',',
        dtype=rides_dtype,
        parse_dates=['quote_date', 'completed_date']
    )

    insert_rides_df(rides_df)

    conn = psycopg2.connect(CONFIG["postgres_url"])

    with conn:
        cur = conn.cursor()
        cur.execute("SELECT * FROM cp_datawarehouse.rides")
        result = cur.fetchall()

    result_df = pd.DataFrame(
        result,
        columns=['ride_id', 'user_id', 'from_zipcode', 'to_zipcode', 'state', 'quote_date', 'completed_date',
                 'price_nominal', 'loyalty_points_earned']
    )
    result_df.astype(
        dtype=rides_dtype,
        parse_dates=['quote_date', 'completed_date']
    )
    result_df['price_nominal'] = result_df['price_nominal'].astype('float64')

    assert_frame_equal(
        rides_df.sort_index(axis=1),
        result_df.sort_index(axis=1),
        check_names=True
    )
