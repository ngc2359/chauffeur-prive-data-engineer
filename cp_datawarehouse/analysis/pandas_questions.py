import logging
import matplotlib.pyplot as plt
import pandas as pd
from cp_datawarehouse.etl.transform import clean_users_csv
from cycler import cycler

LOGGER = logging.getLogger(__name__)


def question_1(users_csv, rides_csv, delimiter=','):
    """
    Answers the first question.
    :param users_csv: the CSV object (path or StringIO) WHICH INCLUDES HEADER ROW
    :param rides_csv: the CSV object (path or StringIO) WHICH INCLUDES HEADER ROW
    :param delimiter: delimiter for pd.read_csv
    :return: a dataframe listing all the users, with the following columns: user_id loyalty_status loyalty_status_txt
                daily_date: date of ride day nb_rides: number of completed rides made by the user for the given day
                total_price: total ride price spent by the user for the given day
    """
    result_df = pd.DataFrame()

    try:
        users_df = clean_users_csv(users_csv)

        rides_df = pd.read_csv(rides_csv, delimiter=delimiter, dtype=object)[
            ['user_id', 'completed_date', 'price_nominal']
        ]
        rides_df['completed_date'] = pd.to_datetime(rides_df['completed_date']).apply(lambda x: x.date())

        agg_function = {
            'user_id': ['count'],
            'loyalty_status': ['max'],
            'loyalty_status_txt': ['max'],
            'price_nominal': ['sum']
        }

        agg_df = pd.merge(users_df, rides_df, on='user_id').groupby(['user_id', 'completed_date']).agg(agg_function)
        agg_df.index.names = ['idx1', 'idx2']

        cols = ['idx1', 'loyalty_status', 'loyalty_status_txt', 'idx2', 'user_id', 'price_nominal']
        result_df = agg_df.reset_index()[cols].rename(
            columns={'idx1': 'user_id', 'idx2': 'daily_date', 'user_id': 'nb_rides', 'price_nominal': 'total_price'}
        )
    except IOError:
        LOGGER.error("Error: unable to read {users_path} or {rides_path}"
                     .format(users_path=users_csv, rides_path=rides_csv))
    else:
        LOGGER.info("Successfully merged {users_path} and {rides_path} to answer question 1"
                    .format(users_path=users_csv, rides_path=rides_csv))

    return result_df


def question_2(rides_csv, delimiter=','):
    """
    Answers the second question.
    :param rides_csv: the CSV object (path or StringIO) WHICH INCLUDES HEADER ROW
    :param delimiter: delimiter for pd.read_csv
    :return: a dataframe listing the average basket per day. The average basket is the average completed ride price
                for a given period of time.
    """
    result_df = pd.DataFrame()

    try:
        rides_df = pd.read_csv(rides_csv, delimiter=delimiter)[
            ['completed_date', 'price_nominal']
        ]
        rides_df['completed_date'] = pd.to_datetime(rides_df['completed_date']).apply(lambda x: x.date())

        agg_df = rides_df.groupby('completed_date').mean()

        result_df = agg_df.reset_index().rename(
            columns={'completed_date': 'daily_date', 'price_nominal': 'avg_basket'}
        )
    except IOError:
        LOGGER.error("Error: unable to read {rides_path}".format(rides_path=rides_csv))
    else:
        LOGGER.info("Successfully aggregated {rides_path} to answer question 2".format(rides_path=rides_csv))

    return result_df


def question_3(rides_csv, delimiter=','):
    """
    Answers the third question.
    :param rides_csv: the CSV object (path or StringIO) WHICH INCLUDES HEADER ROW
    :param delimiter: delimiter for pd.read_csv
    :return: a dataframe listing the 5 days with the lowest number of completed rides, ordered chronologically.
    """
    result_df = pd.DataFrame()

    try:
        rides_df = pd.read_csv(rides_csv, delimiter=delimiter, dtype=object)[
            ['completed_date']
        ]
        rides_df['completed_date'] = pd.to_datetime(rides_df['completed_date']).apply(lambda x: x.date())

        agg_function = {
            'completed_date': ['count']
        }

        agg_df = rides_df.groupby('completed_date').agg(agg_function)

        result_df = agg_df \
            .sort_values(by=[('completed_date', 'count')]) \
            .head(5) \
            .sort_index() \
            .reset_index() \
            .rename(columns={'completed_date': 'daily_date'})
    except IOError:
        LOGGER.error("Error: unable to read {rides_path}".format(rides_path=rides_csv))
    else:
        LOGGER.info("Successfully aggregated {rides_path} to answer question 3".format(rides_path=rides_csv))

    return result_df


def question_4(users_csv, rides_csv, delimiter=','):
    """
    Answers the fourth question.
    :param users_csv: the CSV object (path or StringIO) WHICH INCLUDES HEADER ROW
    :param rides_csv: the CSV object (path or StringIO) WHICH INCLUDES HEADER ROW
    :param delimiter: delimiter for pd.read_csv
    :return: None
    """
    try:
        users_df = pd.read_csv(users_csv, delimiter=delimiter)[
            ['user_id', 'loyalty_status']
        ]

        rides_df = pd.read_csv(rides_csv, delimiter=delimiter)[
            ['user_id', 'completed_date']
        ].dropna(subset=['completed_date'])
        rides_df['year'] = pd.to_datetime(rides_df['completed_date']).dt.year
        rides_df['week'] = pd.to_datetime(rides_df['completed_date']).dt.week
        rides_df.drop(columns=['completed_date'], inplace=True)

        agg_function = {
            'loyalty_status': ['count']
        }

        agg_df = pd.merge(users_df, rides_df, on='user_id')
        agg_df.drop(columns=['user_id'], inplace=True)
        agg_df = agg_df.groupby(['year', 'week', 'loyalty_status']).agg(agg_function)

        plt.rc('axes', prop_cycle=cycler('color', ['red', 'silver', 'gold', 'navy']))
        plt.rc('figure', figsize=(12, 5))

        ax = agg_df.unstack(level=2).plot()
        ax.set_xlabel('week')
        ax.set_ylabel('completed rides')
        ax.legend(['red', 'silver', 'gold', 'platinum'])
        ax.set_title('Number of completed rides per week for each loyalty status')

        plt.show()
    except IOError:
        LOGGER.error("Error: unable to read {rides_path}".format(rides_path=rides_csv))
    else:
        LOGGER.info("Successfully aggregated {rides_path} to answer question 4".format(rides_path=rides_csv))
