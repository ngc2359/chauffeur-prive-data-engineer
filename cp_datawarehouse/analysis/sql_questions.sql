-- Question 1: a table listing all the users, with the following columns: user_id loyalty_status loyalty_status_txt
--              daily_date: date of ride day nb_rides: number of completed rides made by the user for the given day
--              total_price: total ride price spent by the user for the given day
WITH question1
AS (
	SELECT cp_datawarehouse.users.user_id, loyalty_status, loyalty_status_txt, DATE(completed_date) AS daily_date, COUNT(completed_date) AS nb_rides, SUM(price_nominal) as total_price
	FROM cp_datawarehouse.users
	LEFT JOIN cp_datawarehouse.rides ON cp_datawarehouse.users.user_id = cp_datawarehouse.rides.user_id
	GROUP BY (cp_datawarehouse.users.user_id, daily_date)
	)
SELECT *
FROM question1
WHERE daily_date IS NOT NULL;


-- Question 2: a table listing the average basket per day. The average basket is the average completed ride price
--             for a given period of time.
SELECT DATE(completed_date) AS daily_date, AVG(price_nominal) AS avg_basket
FROM cp_datawarehouse.rides
GROUP BY daily_date;

-- Question 3: a table listing the 5 days with the lowest number of completed rides, ordered chronologically.
WITH number_of_rides
AS (
	SELECT DATE(completed_date) AS daily_date, COUNT(DATE(completed_date)) AS nb_rides
	FROM cp_datawarehouse.rides
	GROUP BY daily_date
	), lowest_number_of_rides
AS (
	SELECT *
	FROM number_of_rides
	WHERE daily_date IS NOT NULL
	ORDER BY nb_rides
	LIMIT 5
	)
SELECT *
FROM lowest_number_of_rides
ORDER BY daily_date;
