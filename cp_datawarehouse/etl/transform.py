"""
Transform module provides functions to transform and clean data before further import into cp_datawarehouse tables
"""

import csv
import io
import logging
import pandas as pd

LOGGER = logging.getLogger(__name__)


def clean_users_csv(users_csv, delimiter=','):
    """
    Clean user CSV file values replacing misspelled 'platinium' values into 'platinum'.
    :param list users_csv: the CSV object (path or StringIO) WHICH INCLUDES HEADER ROW
    :param delimiter: delimiter for pd.read_csv
    :return: the cleaned csv as a Pandas dataframe
    """

    users_df = pd.read_csv(users_csv, delimiter=delimiter)
    LOGGER.info(
        "Successfully read {shape} CSV (row(s), column(s)) into dataframe".format(
            shape=users_df.shape
        ))

    users_df['loyalty_status_txt'].replace('platinium', 'platinum', inplace=True)

    return users_df


def filter_rides_csv(rides_csv, delimiter=','):
    """
    Filter rides CSV file rows only keeping rides in Ile-de-France.
    :param rides_csv: the CSV object (path or StringIO) WHICH INCLUDES HEADER ROW
    :param delimiter: delimiter for pd.read_csv
    :return: the filtered csv as a Pandas dataframe
    """
    idf_zipcodes = ('75', '77', '78', '91', '92', '93', '94', '95')
    rides_idf_df = pd.DataFrame()

    try:
        rides_df = pd.read_csv(rides_csv, delimiter=delimiter, dtype=object)
        rides_idf_df = rides_df[rides_df['from_zipcode'].str.strip().str.startswith(idf_zipcodes) &
                                rides_df['to_zipcode'].str.strip().str.startswith(idf_zipcodes)].reset_index(drop=True)
    except IOError:
        LOGGER.error("Error: unable to read {rides_path}".format(rides_path=rides_csv))
    else:
        LOGGER.info("Successfully filtered rides in Ile-de-France: {filtered_lines} out of {total_lines}".format(
            filtered_lines=len(rides_idf_df.index),
            total_lines=len(rides_df.index)
        ))

    return rides_idf_df
