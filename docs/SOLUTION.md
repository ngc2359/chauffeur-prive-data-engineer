# Chauffeur Privé's Data Engineer Take Home Test
> This document provides an overview of the methodology behind the resolution of Chauffeur Privé's Take Home Test.

![Chauffeur Privé - Logo](./logo.png)

The test involves:
* writing clean ETL python code and associated meaningful tests
* writing data analysis scripts using the `pandas` library
* writing data analysis SQL queries

Let's dive in.

## Data transform and load
1. Explore all files in the `datawarehouse/` project top directory and try to understand them.

Below is the tree structure of the project top directory:
```
.
├── cp_datawarehouse
│   ├── config
│   │   ├── base.py
│   │   ├── __init__.py
│   │   └── __pycache__
│   │       ├── base.cpython-35.pyc
│   │       └── __init__.cpython-35.pyc
│   ├── db_models
│   │   ├── create_table_rides.sql
│   │   └── create_table_users.sql
│   ├── etl
│   │   ├── __init__.py
│   │   ├── load.py
│   │   ├── __pycache__
│   │   │   ├── __init__.cpython-35.pyc
│   │   │   ├── load.cpython-35.pyc
│   │   │   └── transform.cpython-35.pyc
│   │   └── transform.py
│   └── __pycache__
├── docker-compose.yml
├── Dockerfile
├── raw_data
│   ├── rides.csv
│   └── users.csv
├── README.md
├── requirements.txt
└── tests
    ├── config
    │   ├── __init__.py
    │   └── test_config.py
    ├── etl
    │   ├── __init__.py
    │   ├── __pycache__
    │   │   ├── __init__.cpython-35.pyc
    │   │   ├── test_load.cpython-35-PYTEST.pyc
    │   │   └── test_transform.cpython-35-PYTEST.pyc
    │   ├── test_load.py
    │   └── test_transform.py
    ├── __init__.py
    ├── __pycache__
    │   └── __init__.cpython-35.pyc
    └── tools
        ├── db_init.py
        ├── __init__.py
        └── __pycache__
            ├── db_init.cpython-35.pyc
            └── __init__.cpython-35.pyc

```

* `cp_datawarehouse`  contains scripts to work with Chauffeur Privé's datawarehouse (python and SQL scripts)
  * `config` contains a configuration object to connect to the provided PostgreSQL container
  * `db_models` contains SQL scripts to create the tables `users` and `rides`
  * `etl` contains python scripts to load and transform data from CSV files
* `raw_data` contains raw CSV files with users and rides data
* `tests` contains `pytest`scripts 
  * `config` contains a script to unit test the configuration declaration
  * `etl` contains scripts to unit test our ETL scripts
  * `tools`contains a script to initialize the `cp_datawarehouse` database

2. Based on `raw_data/rides.csv` input values, write a Python function in `datawarehouse/etl/transform.py` to clean rides by keeping only the rides in the **Île-de-France** region.

For this question we're assuming *"in the Île-de-France region"* means that the ride has to start from the region and end in the region. We then have to only keep rows where `from_zipcode` **and** `to_zipcode` begin with :
* `75`:  Paris
* `77`: Seine-et-Marne
* `78`: Yvelines
* `91`: Essonne
* `92`: Hauts-de-Seine
* `93`: Seine-Saint-Denis
* `94`: Val-de-Marne
* `95`: Val-d'Oise

*Side note: we have to be careful and strip the values in those columns since some zipcodes can contain leading spaces.*

Here is the code below:
```python
def filter_rides_csv(rides_csv, delimiter=','):
    """
    Filter rides CSV file rows only keeping rides in Ile-de-France.
    :param rides_csv: the CSV object (path or StringIO) WHICH INCLUDES HEADER ROW
    :param delimiter: delimiter for pd.read_csv
    :return: the filtered csv as a Pandas dataframe
    """
    idf_zipcodes = ('75', '77', '78', '91', '92', '93', '94', '95')
    rides_idf_df = pd.DataFrame()

    try:
        rides_df = pd.read_csv(rides_csv, delimiter=delimiter, dtype=object)
        rides_idf_df = rides_df[rides_df['from_zipcode'].str.strip().str.startswith(idf_zipcodes) &
                                rides_df['to_zipcode'].str.strip().str.startswith(idf_zipcodes)].reset_index(drop=True)
    except IOError:
        LOGGER.error("Error: unable to read {rides_path}".format(rides_path=rides_csv))
    else:
        LOGGER.info("Successfully filtered rides in Ile-de-France: {filtered_lines} out of {total_lines}".format(
            filtered_lines=len(rides_idf_df.index),
            total_lines=len(rides_df.index)
        ))

    return rides_idf_df
```
*Side note: we will write PEP-8 compliant code though the Markdown renderer will wrap lines sooner.*

3. Write a Python function to load data from a Pandas dataframe into a `cp_datawarehouse.rides` PostgreSQL table.

Loading data from a pandas DataFrame is straightforward using the `df.to_sql` method.

Here is the code below:
```python
def insert_rides_df(rides_df):
    """
    Insert into rides database table the values given as a pandas DataFrame.
    :param rides_df: a pandas DataFrame containing rides data
    """

    engine = create_engine(CONFIG['postgres_url'])
    rides_df.to_sql(schema='cp_datawarehouse', name='rides', con=engine, if_exists='append', index=False)
```

4. Write tests for every function and run them with `pytest` to make sure they pass successfully.

Below are the test functions for `filter_rides_csv` and `insert_rides_csv`:
```python
def test_filter_rides_csv():
    """
    Test the filtering of rides CSV data:
    * should filter zipcodes to only keep Ile-de-France data
    """

    rides_csv = io.StringIO(
        'ride_id,user_id,from_zipcode,to_zipcode,state,quote_date,completed_date,price_nominal,loyalty_points_earned\n'
        '3d08e7b630d78e2272f36e5c6624a108,7fb2935d9b84e0ddb5b95b05a04d224e, 75001,75017,not_completed,2018-04-11 00:15:55.404,,4.00,0\n'
        '01b07272321ef0c57d6ba08518008113,4acfbfe47295ffa624184ae7d0601246,92200,mad,completed,2018-05-03 10:35:50.551,2018-05-03 11:15:10.232,5.74,13\n'
        'c8dedee7af6cf87dad2ed65df81dc2e1,8af8011d328c7e230dad6dc6023c653f,69125,69600,completed,2018-04-28 11:01:32.45,2018-04-28 11:42:15.375,10.78,11\n'
        '51b2610e42c21990c3731335fe961108,3f6286c2b7bcc79831f8ab2cf700c411,69125,75010,not_completed,2018-03-17 21:23:44.798,,4.00,0')

    filtered_rides_df = filter_rides_csv(rides_csv).fillna('')

    assert_frame_equal(
        filtered_rides_df.sort_index(axis=1),
        DataFrame({
            'ride_id': ['3d08e7b630d78e2272f36e5c6624a108'],
            'user_id': ['7fb2935d9b84e0ddb5b95b05a04d224e'],
            'from_zipcode': [' 75001'],
            'to_zipcode': ['75017'],
            'state': ['not_completed'],
            'quote_date': ['2018-04-11 00:15:55.404'],
            'completed_date': [''],
            'price_nominal': ['4.00'],
            'loyalty_points_earned': ['0']
        }).sort_index(axis=1),
        check_names=True
    )
```
Here we're testing the 4 possible outcomes from the truth table of comparing two variables.

```python
def test_insert_rides_df():
    """
    Test INSERT of a pandas DataFrame into rides database table
    """

    initialize_database(drop=True)

    rides_dtype = {
        'ride_id': str,
        'user_id': str,
        'from_zipcode': str,
        'to_zipcode': str,
        'state': str,
        'quote_date': str,
        'completed_date': str,
        'price_nominal': float,
        'loyalty_points_earned': int
    }

    rides_csv = io.StringIO(
        'ride_id,user_id,from_zipcode,to_zipcode,state,quote_date,completed_date,price_nominal,loyalty_points_earned\n'
        '3d08e7b630d78e2272f36e5c6624a108,7fb2935d9b84e0ddb5b95b05a04d224e, 75001,75017,not_completed,2018-04-11 00:15:55.404,,4.00,0\n'
        '01b07272321ef0c57d6ba08518008113,4acfbfe47295ffa624184ae7d0601246,92200,mad,completed,2018-05-03 10:35:50.551,2018-05-03 11:15:10.232,5.74,13\n'
        'c8dedee7af6cf87dad2ed65df81dc2e1,8af8011d328c7e230dad6dc6023c653f,69125,69600,completed,2018-04-28 11:01:32.45,2018-04-28 11:42:15.375,10.78,11\n'
        '51b2610e42c21990c3731335fe961108,3f6286c2b7bcc79831f8ab2cf700c411,69125,75010,not_completed,2018-03-17 21:23:44.798,,4.00,0')

    rides_df = pd.read_csv(
        rides_csv,
        delimiter=',',
        dtype=rides_dtype,
        parse_dates=['quote_date', 'completed_date']
    )

    insert_rides_df(rides_df)

    conn = psycopg2.connect(CONFIG["postgres_url"])

    with conn:
        cur = conn.cursor()
        cur.execute("SELECT * FROM cp_datawarehouse.rides")
        result = cur.fetchall()

    result_df = pd.DataFrame(
        result,
        columns=['ride_id', 'user_id', 'from_zipcode', 'to_zipcode', 'state', 'quote_date', 'completed_date',
                 'price_nominal', 'loyalty_points_earned']
    )
    result_df.astype(
        dtype=rides_dtype,
        parse_dates=['quote_date', 'completed_date']
    )
    result_df['price_nominal'] = result_df['price_nominal'].astype('float64')

    assert_frame_equal(
        rides_df.sort_index(axis=1),
        result_df.sort_index(axis=1),
        check_names=True
    )
```
Here we're inserting a sample from the CSV file and comparing the result of a simple SQL query to the sample before insertion.

## Python Pandas
The answers for the following questions are located in the`datawarehouse/cp_datawarehouse/analysis/` directory.

1. Write a function which returns a dataframe listing all the users, with the following columns: `user_id` `loyalty_status` `loyalty_status_txt` `daily_date`: date of ride day `nb_rides:` number of completed rides made by the user for the given day `total_price:` total ride price spent by the user for the given day.

Here is the code below:
```python
def question_1(users_csv, rides_csv, delimiter=','):
    """
    Answers the first question.
    :param users_csv: the CSV object (path or StringIO) WHICH INCLUDES HEADER ROW
    :param rides_csv: the CSV object (path or StringIO) WHICH INCLUDES HEADER ROW
    :param delimiter: delimiter for pd.read_csv
    :return: a dataframe listing all the users, with the following columns: user_id loyalty_status loyalty_status_txt
                daily_date: date of ride day nb_rides: number of completed rides made by the user for the given day
                total_price: total ride price spent by the user for the given day
    """
    result_df = pd.DataFrame()

    try:
        users_df = clean_users_csv(users_csv)

        rides_df = pd.read_csv(rides_csv, delimiter=delimiter, dtype=object)[
            ['user_id', 'completed_date', 'price_nominal']
        ]
        rides_df['completed_date'] = pd.to_datetime(rides_df['completed_date']).apply(lambda x: x.date())

        agg_function = {
            'user_id': ['count'],
            'loyalty_status': ['max'],
            'loyalty_status_txt': ['max'],
            'price_nominal': ['sum']
        }

        agg_df = pd.merge(users_df, rides_df, on='user_id').groupby(['user_id', 'completed_date']).agg(agg_function)
        agg_df.index.names = ['idx1', 'idx2']

        cols = ['idx1', 'loyalty_status', 'loyalty_status_txt', 'idx2', 'user_id', 'price_nominal']
        result_df = agg_df.reset_index()[cols].rename(
            columns={'idx1': 'user_id', 'idx2': 'daily_date', 'user_id': 'nb_rides', 'price_nominal': 'total_price'}
        )
    except IOError:
        LOGGER.error("Error: unable to read {users_path} or {rides_path}"
                     .format(users_path=users_csv, rides_path=rides_csv))
    else:
        LOGGER.info("Successfully merged {users_path} and {rides_path} to answer question 1"
                    .format(users_path=users_csv, rides_path=rides_csv))

    return result_df
```

2. Write a function which returns a dataframe listing the average basket per day. The average basket is the average completed ride price for a given period of time. 

Here is the code below:
```python
def question_2(rides_csv, delimiter=','):
    """
    Answers the second question.
    :param rides_csv: the CSV object (path or StringIO) WHICH INCLUDES HEADER ROW
    :param delimiter: delimiter for pd.read_csv
    :return: a dataframe listing the average basket per day. The average basket is the average completed ride price
                for a given period of time.
    """
    result_df = pd.DataFrame()

    try:
        rides_df = pd.read_csv(rides_csv, delimiter=delimiter)[
            ['completed_date', 'price_nominal']
        ]
        rides_df['completed_date'] = pd.to_datetime(rides_df['completed_date']).apply(lambda x: x.date())

        agg_df = rides_df.groupby('completed_date').mean()

        result_df = agg_df.reset_index().rename(
            columns={'completed_date': 'daily_date', 'price_nominal': 'avg_basket'}
        )
    except IOError:
        LOGGER.error("Error: unable to read {rides_path}".format(rides_path=rides_csv))
    else:
        LOGGER.info("Successfully aggregated {rides_path} to answer question 2".format(rides_path=rides_csv))

    return result_df
```

3. Write a function which returns a dataframe listing the 5 days with the lowest number of completed rides, ordered chronologically.

Here is the code below:
```python
def question_3(rides_csv, delimiter=','):
    """
    Answers the third question.
    :param rides_csv: the CSV object (path or StringIO) WHICH INCLUDES HEADER ROW
    :param delimiter: delimiter for pd.read_csv
    :return: a dataframe listing the 5 days with the lowest number of completed rides, ordered chronologically.
    """
    result_df = pd.DataFrame()

    try:
        rides_df = pd.read_csv(rides_csv, delimiter=delimiter, dtype=object)[
            ['completed_date']
        ]
        rides_df['completed_date'] = pd.to_datetime(rides_df['completed_date']).apply(lambda x: x.date())

        agg_function = {
            'completed_date': ['count']
        }

        agg_df = rides_df.groupby('completed_date').agg(agg_function)

        result_df = agg_df \
            .sort_values(by=[('completed_date', 'count')]) \
            .head(5) \
            .sort_index() \
            .reset_index() \
            .rename(columns={'completed_date': 'daily_date'})
    except IOError:
        LOGGER.error("Error: unable to read {rides_path}".format(rides_path=rides_csv))
    else:
        LOGGER.info("Successfully aggregated {rides_path} to answer question 3".format(rides_path=rides_csv))

    return result_df
```

4. Create a chart plotting the number of completed rides per week for each loyalty status.

Here is the code below:
```python
def question_4(users_csv, rides_csv, delimiter=','):
    """
    Answers the fourth question.
    :param users_csv: the CSV object (path or StringIO) WHICH INCLUDES HEADER ROW
    :param rides_csv: the CSV object (path or StringIO) WHICH INCLUDES HEADER ROW
    :param delimiter: delimiter for pd.read_csv
    :return: None
    """
    try:
        users_df = pd.read_csv(users_csv, delimiter=delimiter)[
            ['user_id', 'loyalty_status']
        ]

        rides_df = pd.read_csv(rides_csv, delimiter=delimiter)[
            ['user_id', 'completed_date']
        ].dropna(subset=['completed_date'])
        rides_df['year'] = pd.to_datetime(rides_df['completed_date']).dt.year
        rides_df['week'] = pd.to_datetime(rides_df['completed_date']).dt.week
        rides_df.drop(columns=['completed_date'], inplace=True)

        agg_function = {
            'loyalty_status': ['count']
        }

        agg_df = pd.merge(users_df, rides_df, on='user_id')
        agg_df.drop(columns=['user_id'], inplace=True)
        agg_df = agg_df.groupby(['year', 'week', 'loyalty_status']).agg(agg_function)

        plt.rc('axes', prop_cycle=cycler('color', ['red', 'silver', 'gold', 'navy']))
        plt.rc('figure', figsize=(12, 5))

        ax = agg_df.unstack(level=2).plot()
        ax.set_xlabel('week')
        ax.set_ylabel('completed rides')
        ax.legend(['red', 'silver', 'gold', 'platinum'])
        ax.set_title('Number of completed rides per week for each loyalty status')

        plt.show()
    except IOError:
        LOGGER.error("Error: unable to read {rides_path}".format(rides_path=rides_csv))
    else:
        LOGGER.info("Successfully aggregated {rides_path} to answer question 4".format(rides_path=rides_csv))
```

## SQL
Here are the corresponding SQL queries:
```sql
-- Question 1: a table listing all the users, with the following columns: user_id loyalty_status loyalty_status_txt
--              daily_date: date of ride day nb_rides: number of completed rides made by the user for the given day
--              total_price: total ride price spent by the user for the given day
WITH question_1
AS (
	SELECT cp_datawarehouse.users.user_id, loyalty_status, loyalty_status_txt, DATE(completed_date) AS daily_date, COUNT(completed_date) AS nb_rides, SUM(price_nominal) as total_price
	FROM cp_datawarehouse.users
	LEFT JOIN cp_datawarehouse.rides ON cp_datawarehouse.users.user_id = cp_datawarehouse.rides.user_id
	GROUP BY (cp_datawarehouse.users.user_id, daily_date)
	)
SELECT *
FROM question_1
WHERE daily_date IS NOT NULL;


-- Question 2: a table listing the average basket per day. The average basket is the average completed ride price
--             for a given period of time.
SELECT DATE(completed_date) AS daily_date, AVG(price_nominal) AS avg_basket
FROM cp_datawarehouse.rides
GROUP BY daily_date;

-- Question 3: a table listing the 5 days with the lowest number of completed rides, ordered chronologically.
WITH number_of_rides
AS (
	SELECT DATE(completed_date) AS daily_date, COUNT(DATE(completed_date)) AS nb_rides
	FROM cp_datawarehouse.rides
	GROUP BY daily_date
	), lowest_number_of_rides
AS (
	SELECT *
	FROM number_of_rides
	WHERE daily_date IS NOT NULL
	ORDER BY nb_rides
	LIMIT 5
	)
SELECT *
FROM lowest_number_of_rides
ORDER BY daily_date;
```
*Side note: to test out SQL queries, I just mounted the `raw_data` directory as a volume of `postgres-dw` and used the following queries to import data:*
```sql
COPY cp_datawarehouse.users FROM '/raw_data/users.csv' WITH (FORMAT csv, HEADER true);
COPY cp_datawarehouse.rides FROM '/raw_data/rides.csv' WITH (FORMAT csv, HEADER true);
```

Thank you for reading it through!

**For more information, please contact me at [marc.mla@protonmail.com](mailto:marc.mla@protonmail.com).**